package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoAlreadyCompletedException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class ToDoController {

	@Autowired
	ToDoService toDoService;
	
	@GetMapping("/todos")
	@Valid List<ToDoResponse> getAll(@RequestParam(required = false) String date) {
		return (date == null) ?
				toDoService.getAll()
				:
				toDoService.getAllByDay(LocalDate.parse(date));
	}

	@PutMapping("/todos")
	@Valid ToDoResponse save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) throws ToDoNotFoundException {
		return toDoService.upsert(todoSaveRequest);
	}

	@PostMapping("/todos/{id}/complete")
	@Valid ToDoResponse setAsCompleted(@PathVariable Long id) throws ToDoNotFoundException, ToDoAlreadyCompletedException {
		ToDoResponse k = toDoService.completeToDo(id);
		return toDoService.completeToDo(id);
	}

	@PostMapping("/todos/{id}/incomplete")
	@Valid ToDoResponse setAsIncompleted(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.incomplete(id);
	}

	@GetMapping("/todos/{id}")
	@Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.getOne(id);
	}

	@DeleteMapping("/todos/{id}")
	void delete(@PathVariable Long id) {
		toDoService.deleteOne(id);
	}

	@DeleteMapping("/todos/")
	void deleteList(@RequestBody List<Long> id) {
		toDoService.deleteList(id);
	}

	@ExceptionHandler(ToDoNotFoundException.class)
	ResponseEntity<Object> handleException(Exception e) {
		return ResponseEntity.notFound().build();
	}

}