package com.example.demo.exception;

/**
 * 12.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
public class ToDoAlreadyCompletedException extends Exception {

    public ToDoAlreadyCompletedException(Long id) {
        super(String.format("Trying to complete already completed To Do with id: %d", id));
    }
}
