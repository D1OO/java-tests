package com.example.demo.model;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * ToDoEntity
 */
@Entity
public class ToDoEntity {

	@Id
	@NotNull
	@GeneratedValue
	private Long id;

	@Basic
	@NotNull
	private String text;

	@Basic
	private ZonedDateTime completedAt;

	@Basic
	private LocalDate createdAt;

	public ToDoEntity() {}

	public ToDoEntity(@NotNull Long id) {
		this.id = id;
	}

	public ToDoEntity(@NotNull Long id, @NotNull String text) {
		this.id = id;
		this.text = text;
	}

	public ToDoEntity(@NotNull String text, LocalDate createdAt) {
		this.createdAt = createdAt;
		this.text = text;
	}

	public ToDoEntity(@NotNull Long id, @NotNull String text, LocalDate createdAt) {
		this.createdAt = createdAt;
		this.id = id;
		this.text = text;
	}

	public ToDoEntity(@NotNull Long id, @NotNull String text, ZonedDateTime completedAt, LocalDate createdAt) {
		this.id = id;
		this.text = text;
		this.completedAt = completedAt;
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return String.format(
			"ToDoEntity[id=%d, text='%s', completedAt='%s']",
			id, text, completedAt.toString()
		);
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public ToDoEntity setText(String text) {
		this.text = text;
		return this;
	}

	public ZonedDateTime getCompletedAt() {
		return completedAt;
	}

	public ToDoEntity completeNow() {
		completedAt = ZonedDateTime.now(ZoneOffset.UTC);
		return this;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCompletedAt(ZonedDateTime completedAt) {
		this.completedAt = completedAt;
	}
}