package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Autowired
    private ObjectMapper jackson;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        when(toDoRepository.findAll()).thenReturn(
                Arrays.asList(
                        new ToDoEntity(1l, testText)
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenNotFound_thenThrowException() throws Exception {
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

        this.mockMvc
                .perform(get("/todos/99"))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenGetAllByDay_thenReturnValidResponse() throws Exception {
        LocalDate date = LocalDate.now().minusDays(10);

        when(toDoRepository.findAllByCreatedAt(ArgumentMatchers.any(LocalDate.class))).thenReturn(
                Arrays.asList(
                        new ToDoEntity(1L, "Test 1", date),
                        new ToDoEntity(1L, "Test 2", date),
                        new ToDoEntity(1L, "Test 3", date)
                )
        );

        String response = this.mockMvc
                .perform(get("/todos?date=" + date.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<ToDoEntity> data = jackson.readValue(response, new TypeReference<>() {
        });

        data.forEach(e -> assertEquals(e.getCreatedAt(), date));
    }

    @Test
    void whenDeleteOne_thenDeleteCalled() throws Exception {
        this.mockMvc.perform(delete("/todos/10"));

        verify(toDoRepository, times(1)).deleteById(10L);
    }

}
