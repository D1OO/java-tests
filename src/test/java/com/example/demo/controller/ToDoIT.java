package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 12.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ToDoIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @Test
    void whenDeleteList_thenReturnValidResponse() throws Exception {

        //mock (Fill Repository with data)
        List<ToDoSaveRequest> repoInitData = List.of(
                new ToDoSaveRequest(null, "Test 0"),
                new ToDoSaveRequest(null, "Test 1"),
                new ToDoSaveRequest(null, "Test 2"),
                new ToDoSaveRequest(null, "Test 3"),
                new ToDoSaveRequest(null, "Test 4"),
                new ToDoSaveRequest(null, "Test 5"));
        repoInitData.forEach(d -> {
            try {
                mockMvc.perform(put("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(d)));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        String response1 = this.mockMvc.perform(get("/todos")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        List<ToDoEntity> repoBeforeDeleteList = jackson.readValue(response1, new TypeReference<>() {
        });
        assertEquals(repoInitData.size(), repoBeforeDeleteList.size());


        //call (Delete Test 1, Test 3, Test 4)
        List<Long> toDelete = List.of(repoBeforeDeleteList.get(1).getId(),
                repoBeforeDeleteList.get(3).getId(),
                repoBeforeDeleteList.get(4).getId());
        this.mockMvc.perform(delete("/todos/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jackson.writeValueAsString(toDelete)))
                .andExpect(status().isOk());


        //validate
        String response2 = this.mockMvc.perform(get("/todos")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        List<ToDoEntity> repoAfterDeleteList = jackson.readValue(response2, new TypeReference<>() {
        });

        assertEquals(repoAfterDeleteList.size(), repoBeforeDeleteList.size() - toDelete.size());
        assertEquals("Test 0", repoAfterDeleteList.get(0).getText());
        assertEquals("Test 2", repoAfterDeleteList.get(1).getText());
        assertEquals("Test 5", repoAfterDeleteList.get(2).getText());
    }
}
