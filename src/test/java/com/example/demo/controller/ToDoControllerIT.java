package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Autowired
    private ObjectMapper jackson;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        Long testId = 1l;
        when(toDoService.getAll()).thenReturn(
                Arrays.asList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenComplete_thenSetValidResponse() throws Exception {
        var startTime = ZonedDateTime.now(ZoneOffset.UTC);

        when(toDoService.completeToDo(anyLong()))
                .thenReturn(ToDoEntityToResponseMapper
                        .map(new ToDoEntity(10L, "Test 10", ZonedDateTime.now(), LocalDate.now())));

        String response = this.mockMvc
                .perform(post("/todos/10/complete"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        ToDoResponse result = jackson.readValue(response, ToDoResponse.class);

        assertTrue(result.completedAt.isAfter(startTime));
    }

}
